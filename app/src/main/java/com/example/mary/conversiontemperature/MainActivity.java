package com.example.mary.conversiontemperature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;


import static android.R.attr.value;

public class MainActivity extends AppCompatActivity {

    EditText enterTemp;
    RadioButton toCelsiusRadioButton;
    RadioButton toFahrenheitRadioButton;
    Button buttonConvert;
    TextView ResulttextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enterTemp = (EditText)findViewById(R.id.enterTemp);
        toCelsiusRadioButton = (RadioButton)findViewById(R.id.toCelsiusRadioButton);
        toFahrenheitRadioButton = (RadioButton)findViewById(R.id.toFahrenheitRadioButton);
        buttonConvert = (Button)findViewById(R.id.buttonConvert);
        ResulttextView = (TextView)findViewById(R.id.ResulttextView);

        buttonConvert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                double degree = Double.parseDouble(enterTemp.getText().toString());
                if (toCelsiusRadioButton.isChecked())
                    degree = Converter.ConvertFahrenheitToCelsius(degree);
                else
                    degree = Converter.ConvertCelsiusToFahrenheit(degree);
                ResulttextView.setText(new Double(degree).toString());

            }
            }
        );}
}


class Converter {
    public static double ConvertCelsiusToFahrenheit(double cel)
    {
        return cel * 9 / 5 + 32;
    }

    public static double ConvertFahrenheitToCelsius(double fahr)
    {

        return  (fahr - 32) * 5 / 9;

    }
}